OpenStreetMap change detection tool
===================================

# Introduction
This tool allows to detect and analyse the changes made on a specific OpenStreetMap
tag between 2 dates. It is based on the [augmented diff](https://wiki.openstreetmap.org/wiki/Overpass_API/Augmented_Diffs)
tool of the Overpass API. It takes as input:

* date_ini: Initial date
* date_final: Final date
* watch_tag: Specific OSM tag, expressed as '"key"="value"' or '"key"'
* geom_type: Type of OSM geometry, i.e., `node`, `way` or `relation`  
* area: Name of the area, e.g., `"name:fr" = "Bruxelles-Capitale"` or `"name" = "Berlin"`

# Installation
## Create a virtual environment, using python 3.
```
virtualenv -p python3 venv
```

## Activate the virtual environment
```
source venv/bin/activate
```
## Install requirements
```
pip install -r requirements.txt
```
## Create a postgis db

### via docker, for instance
```
docker run --name firebru-diff-db -p 5432 -d mdillon/postgis
```
## Parameter the db
Copy the `src/parameters.py.dist` into `src/parameters.py` and edit the file `src/parameters.py` with local credentials. If the db was created via docker, type `docker ps` to know the port in use.

## Create the tables in the db
Run `python src/db.py`


# Usage

This tool analyses the [augmented diff](https://wiki.openstreetmap.org/wiki/Overpass_API/Augmented_Diffs) results between two dates for a specific tag of OpenStreetMap. A *query* is defined as an augmented diff between two dates (*date_ini* & *date_final*) for a particular tag (namely, the *watch_tag*) over a certain area (namely, the *area*). Each *query* produces a number of *diffs* that are changes recorded for one particular OSM feature. Changes can be creation, deletion or modification of the geometry or tags of OSM features.


Input parameters are to be changed at the end of `adiff_change_detection.py`

Then, run `python src/adiff_change_detection.py`

Look at the `diff` table in the db to see the changes.

# Documentation

Documentation is managed by Sphinx. Run `make html` or `make latexpdf` in the doc repository to compile the doc.  
