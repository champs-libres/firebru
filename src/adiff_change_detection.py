#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# modules
from osmdiff import AugmentedDiff
from db import Query, Diff, get_session
from osm_request import get_adiff_xml, get_geojson
import json
import datetime
import logging
import os

RESULTS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../results/')

def change_detection(
        watch_tag, geom_type, area, date_ini,
        date_final, watch_tag_value_to_ignore=set()):
    logger = logging.getLogger('main')

    # 1) Make the overpass queries
    # 1.1) the diff query
    adiff_xml = get_adiff_xml(date_ini, date_final, area, geom_type, watch_tag)

    # Store the response into a file (for use with AugmentedDiff)
    xml_file = open(os.path.join(RESULTS_DIR, 'adiff.xml'), 'w+')
    xml_file.write(adiff_xml)
    xml_file.close()

    # 1.2) Get the geometries at date_ini
    geojson_ini = get_geojson(geom_type, watch_tag, area, date_ini)
    # Store the response into a file
    with open(os.path.join(RESULTS_DIR, 'geojson_ini.geojson'), 'w+') as outfile:
        json.dump(geojson_ini, outfile)

    # 1.3) Get the geometries at date_final
    geojson_final = get_geojson(geom_type, watch_tag, area, date_final)
    # Store the response into a file
    with open(os.path.join(RESULTS_DIR, 'geojson_final.geojson'), 'w+') as outfile:
        json.dump(geojson_final, outfile)

    # 2) Run the augmented diff
    a = AugmentedDiff(file=os.path.join(RESULTS_DIR, 'adiff.xml'))

    count_case_create_geom = 0
    count_case_create_tag = 0
    count_case_delete_geom = 0
    count_case_create_geom_split = 0
    count_case_delete_tag = 0
    count_case_mod_geom = 0
    count_case_mod_geom_reverse = 0
    count_case_mod_geom_alignment = 0
    count_case_mod_geom_same_length = 0
    count_case_mod_geom_modif = 0
    count_case_mod_tag = 0
    count_case_mod_target_tag = 0

    # Initiate the session
    db_session = get_session()

    # Store the query in the table
    q = Query(
        watch_tag=watch_tag,
        watch_tag_exception=', '.join(watch_tag_value_to_ignore),
        geom_type=geom_type,
        area=area,
        date_ini=date_ini,
        date_final=date_final,
        created_on=datetime.datetime.now()
    )

    db_session.add(q)
    db_session.commit()

    Diff.configure(db_session, q)

    # Collect the nodes id
    nodes_ref_list = []
    for i in a.create:
        for n in i.nodes:
            nodes_ref_list.append(n.attribs['ref'])

    watch_key = watch_tag.split('=')[0]
    # if '=' in watch_tag: # not used?
    #     watch_value = watch_tag.split('=')[1]

    ######################
    # Analysing creation #
    ######################
    for i in a.create:
        version = i.attribs['version']
        tag_value = i.tags[watch_key]
        if version == '1':
            diff_target = Diff.GEOM_TARGET
            count_case_create_geom += 1

            logger.info('(1) Object created with ' + watch_key + ' = ' + tag_value)

            # TEST if this new geometry is coming from a way-split
            way_split = 0
            for n in i.nodes:
                if nodes_ref_list.count(n.attribs['ref']) > 1:
                    way_split = 1

            count_case_create_geom_split += way_split

            diff_category = 'geom_creation'

            if way_split == 1:
                diff_category = 'way_split'

                logger.info('-> (1.1) Object created with ' + watch_key + ' = ' + tag_value + ', possible way-spliting')

        else:  # version > 1
            diff_target = Diff.TAG_TARGET
            count_case_create_tag += 1

            logger.info('(2) Tag created: ' + watch_key + ' = ' + tag_value)

            diff_category = 'tag_creation'

            # TEST if watch_tag had a particular value
            if i.tags[watch_key] in watch_tag_value_to_ignore:
                diff_category = watch_key + '=' + tag_value

        osm_id = i.attribs['id']
        changeset_id = i.attribs['changeset']
        udate = i.attribs['timestamp']

        if tag_value in watch_tag_value_to_ignore:
            diff_category = watch_key + '=' + tag_value

        Diff.create_and_store(
            osm_id, changeset_id, Diff.CREATE_TYPE, diff_target,
            diff_category, udate=udate, tag_value=tag_value)


    ######################
    # Analysing deletion #
    ######################

    for i in a.delete:
        visible = i['new'].attribs['visible']
        old_tag_value = i['old'].tags[watch_key]
        if visible == 'false':
            count_case_delete_geom += 1
            diff_target = Diff.GEOM_TARGET
            diff_category = 'geom_deletion'

            logger.info('(3) Object deleted')

        else:
            count_case_delete_tag = count_case_delete_tag + 1
            diff_target = Diff.TAG_TARGET
            diff_category = 'tag_deletion'

            logger.info('(4) Tag deleted: ' + watch_key + ' = ' + old_tag_value + ' --> x')

        osm_id = i['old'].attribs['id']
        changeset_id = i['new'].attribs['changeset']
        udate = i['new'].attribs['timestamp']
        previous_udate = i['old'].attribs['timestamp']
        previous_tag_value = old_tag_value

        if previous_tag_value in watch_tag_value_to_ignore:
            diff_category = watch_key + '=' + previous_tag_value

        Diff.create_and_store(
            osm_id, changeset_id, Diff.DELETE_TYPE, diff_target, diff_category,
            udate=udate, previous_udate=previous_udate,
            previous_tag_value=previous_tag_value)


    ##########################
    # Analysing modification #
    ##########################

    for i in a.modify:
        old_tag_value = i['old'].tags[watch_key]
        # analyse tags
        if i['new'].tags == i['old'].tags:
            count_case_mod_geom += 1
            diff_target = Diff.GEOM_TARGET

            logger.info('(5) Modification of geometries only')

            # Si aucun noeud n'a été rajouté (mais seulement déplacés), le contenu de l'objet <way></way> est identique entre l'objet <old><way></way></old> et <new><way></way></new>
            new_n = i['new'].nodes
            old_n = i['old'].nodes
            if len(new_n) == len(old_n):
                ii = 0

                # test if the old and new objects contains the same node ids, in the same order.
                for ii in range(0, len(new_n)):
                    if int(new_n[ii].attribs['ref']) == int(old_n[ii].attribs['ref']):
                        pure_node_align = True
                        reverse_way = False
                    else:
                        pure_node_align = False
                        # test if the old and new objects have different order of nodes (reverse way)
                        if int(new_n[ii].attribs['ref']) == int(old_n[len(new_n)-ii-1].attribs['ref']):
                            reverse_way = True
                        else:
                            reverse_way = False
                            logger.info('nodes are not in the same order between the old and new objects')
                            break

                if pure_node_align:
                    logger.info('-> (5.2) Same nodes, alignment')
                    count_case_mod_geom_alignment += 1
                    diff_category = 'node_align'

                if reverse_way:
                    logger.info('-> (5.3) Same nodes, reverse way')
                    count_case_mod_geom_reverse += 1
                    diff_category = 'reverse_way'

                if (not pure_node_align) & (not reverse_way):
                    logger.info('-> (5.4) Same length, but not same nodes, or nodes in a different order')
                    count_case_mod_geom_same_length += 1
                    diff_category = 'same_length_not_same_nodes'
            else:
                logger.info('--> (5.1) Old and new objects don\'t have the same number of nodes')
                diff_category = 'modif_geom'
                count_case_mod_geom_modif += 1

        else:
            count_case_mod_tag += 1
            diff_target = Diff.TAG_TARGET

            # analyse if oneway tags were modified
            if i['new'].tags[watch_key] == old_tag_value:
                # false positive
                logger.info('(6.1) Tag modifications: not for ' + watch_tag + ', false positive')

                diff_category = 'other_tag_modified'  # 'other_tag_modif'

            else:
                count_case_mod_target_tag += 1
                logger.info('(6) Tag modified: ' + watch_key + ' = ' + i['new'].tags["oneway"] + " --> " + old_tag_value) #TODO check, should be reversed?
                diff_category = 'watch_tag_modified'

        osm_id = i['old'].attribs['id']
        changeset_id = i['new'].attribs['changeset']
        udate = i['new'].attribs['timestamp']
        previous_udate = i['old'].attribs['timestamp']
        tag_value = i['new'].tags[watch_key]
        previous_tag_value = old_tag_value

        Diff.create_and_store(
            osm_id, changeset_id, Diff.MODIFY_TYPE, diff_target, diff_category,
            udate=udate, previous_udate=previous_udate, tag_value=tag_value,
            previous_tag_value=previous_tag_value)


    # write summary file
    summary = open(os.path.join(RESULTS_DIR, 'adiff_summary.txt'), 'w+')
    summary.write('Input parameters\n')
    summary.write('----------------\n')
    summary.write('Objects: ' + geom_type + '["' + watch_tag + '"]\n')
    summary.write('Area: ' + area + '\n')
    summary.write('Changes from ' + date_ini + ' to ' + date_final + '\n')
    summary.write('\n')
    summary.write('Results\n')
    summary.write('-------\n')
    summary.write(str(count_case_create_tag+count_case_create_geom+count_case_delete_tag+count_case_delete_geom+count_case_mod_tag+count_case_mod_geom) + ' changes in total\n')
    summary.write('--> from which: ' + str(count_case_create_tag+count_case_create_geom) + ' creations,\n')
    summary.write('--> from which: ' + str(count_case_delete_tag+count_case_delete_geom) + ' deletions,\n')
    summary.write('--> from which: ' + str(count_case_mod_tag+count_case_mod_geom) + ' modifications.\n')
    summary.write('In details:\n')
    summary.write(str(count_case_create_tag) + ' tag creations\n')
    summary.write(str(count_case_create_geom) + ' geom creations\n')
    summary.write('--> from which: ' + str(count_case_create_geom_split) + ' possibly from way split\n')
    summary.write(str(count_case_delete_tag) + ' tag deletion\n')
    summary.write(str(count_case_delete_geom) + ' geom deletion\n')
    summary.write(str(count_case_mod_tag) + ' tag modifications\n')
    summary.write('--> from which: ' + str(count_case_mod_target_tag) + ' target tag modifications\n')
    summary.write(str(count_case_mod_geom) + ' geom modifications\n')
    summary.write('--> from which: ' + str(count_case_mod_geom_alignment) + ' node alignment,\n')
    summary.write('--> from which: ' + str(count_case_mod_geom_reverse) + ' reverse way,\n')
    summary.write('--> from which: ' + str(count_case_mod_geom_same_length) + ' have the same number of nodes, but not the same nodes,\n')
    summary.write('--> from which: ' + str(count_case_mod_geom_modif) + ' geometry modification by deletion or creation of nodes.\n')
    summary.close()


if __name__ == '__main__':
    import sys

    log_level = logging.CRITICAL
    for arg in sys.argv:
        if arg[:5] == '-log=':
            print('Set log level to "{}"'.format(arg[5:]))
            lvls = {
                'CRITICAL': logging.CRITICAL, 'ERROR': logging.ERROR,
                'WARNING': logging.WARNING, 'INFO': logging.INFO,
                'DEBUG': logging.DEBUG}
            log_level = lvls[arg[5:]]


    # settings
    watch_tag = ['shop', 'amenity=pharmarcy']
    geom_type = 'way'
    area = '"name:fr" = "Habay"'
    date_ini = '2020-04-01T00:00:00Z'
    date_final = '2020-05-01T00:00:00Z'
    watch_tag_value_to_ignore = set(['no'])

    logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(os.path.join(RESULTS_DIR, 'main.log'))
    fh.setLevel(log_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    change_detection(
        watch_tag, geom_type, area, date_ini,
        date_final, watch_tag_value_to_ignore)
