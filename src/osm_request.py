import overpass

# Debug - modification of the overpass.api object to support the use of [adiff]
# or [date] elements
overpass.API._GEOJSON_QUERY_TEMPLATE = "[out:json]{query}out body geom;"
overpass.API._QUERY_TEMPLATE = "[out:{out}]{query}out {verbosity};"


def make_tag_subquery(watch_tag, area, geom_type):
    area_subquery = 'area[{}];{}(area)'.format(area, geom_type)
    if isinstance(watch_tag, list):
        tag_subquery = '(' + area_subquery + \
            '[{}];'.format('];{}['.format(area_subquery).join(watch_tag)) + ');'
    else:
        tag_subquery = '{}[{}]'.format(area_subquery, watch_tag)
    print(tag_subquery)
    return tag_subquery

def get_adiff_xml(date_ini, date_final, area, geom_type, watch_tag):
    """Get a xml showing the Augmented Difference between date_ini and
     date_final, using the overpass API"""

    query = '[adiff:"' + date_ini + '","' + date_final + '"];' + \
        make_tag_subquery(watch_tag, area, geom_type)
    print('Overpass query is: ' + query)

    api = overpass.API(timeout=500)
    # api = overpass.API(endpoint="https://overpass.openstreetmap.fr/api/")
    adiff_xml = api.Get(query, responseformat="xml", verbosity="meta")

    print('Overpass query done')

    return adiff_xml


def get_geojson(geom_type, watch_tag, area, timestamp):
    """Get the geojson object from the geom_type, the watch_tag, the area and
    timestamp using the overpass API"""

    query = '[date:"' + timestamp + '"];' + \
        make_tag_subquery(watch_tag, area, geom_type)
    print('Overpass query is: ' + query)

    api = overpass.API(timeout=500)
    geojson = api.Get(query, responseformat="geojson")

    print('Overpass query done')
    return geojson
