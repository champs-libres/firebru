from sqlalchemy import create_engine, Column, Integer, DateTime, Enum, Text,\
    ForeignKey
import parameters as p
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from geoalchemy2.types import Geometry
import json
import os


RESULTS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../results/')


def get_engine():
    engine = create_engine(
        'postgresql://{}:{}@{}:{}/{}'.format(
            p.db_user, p.db_pwd, p.db_host, p.db_port, p.db_name), echo=False)
    return engine


def get_session():
    # creation de la session
    engine = get_engine()
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


Base = declarative_base()


class Query(Base):
    """ This table stores the parameters of the augmented diff request """
    __tablename__ = 'query'
    id = Column(Integer, primary_key=True)
    watch_tag = Column(Text)
    watch_tag_exception = Column(Text)
    geom_type = Column(Enum('node', 'way', 'relation', name='osm_type'))
    area = Column(Text)
    date_ini = Column(DateTime)
    date_final = Column(DateTime)
    created_on = Column(DateTime)


class Diff(Base):
    __tablename__ = 'diff'
    """ This table stores the results of the augmented diff request as single diff features """
    id = Column(Integer, primary_key=True)
    osm_id = Column(Integer)
    changeset_id = Column(Integer)
    udate = Column(DateTime)
    previous_udate = Column(DateTime)
    geom = Column(Geometry(geometry_type='LINESTRING', srid=4326))
    previous_geom = Column(Geometry(geometry_type='LINESTRING', srid=4326))
    tag_value = Column(Text)
    previous_tag_value = Column(Text)
    diff_type = Column(Enum('create', 'modify', 'delete', name='diff_type'))
    diff_target = Column(Enum('geom', 'tag', 'both', name='diff_target'))
    diff_category = Column(Enum(
        'geom_creation',
        'tag_creation',
        'geom_deletion',
        'tag_deletion',
        'way_split',
        'modif_geom',
        'node_align',
        'reverse_way',
        'same_length_not_same_nodes',
        'watch_tag_modified',
        'other_tag_modified',
        'oneway=no',
        name='diff_category'))
    query_id = Column(Integer, ForeignKey('query.id'))
    query = relationship("Query", back_populates="diff")

    # class element
    session = None
    session_query = None
    CREATE_TYPE = 'create'
    MODIFY_TYPE = 'modify'
    DELETE_TYPE = 'delete'

    GEOM_TARGET = 'geom'
    TAG_TARGET = 'tag'

    @classmethod
    def configure(cls, session, session_query):
        cls.session = session
        cls.session_query = session_query

    @classmethod
    def create_and_store(
            cls,
            osm_id, changeset_id, diff_type, diff_target, diff_category,
            udate=None, previous_udate=None, tag_value=None,
            previous_tag_value=None):

        if not cls.session and cls.session_query:
            raise Exception('You must configure session and session_query\
for the class Diff (via the configure class methode)')

        d = cls(
            osm_id=osm_id,
            changeset_id=changeset_id,
            udate=udate,
            previous_udate=previous_udate,
            diff_type=diff_type,
            diff_target=diff_target,
            diff_category=diff_category,
            tag_value=tag_value,
            previous_tag_value=previous_tag_value,
            query=cls.session_query
        )
        d.load_geoms()

        cls.session.add(d)
        cls.session.commit()

    def load_geoms(self):

        if self.udate:
            if not self.diff_type == 'delete':
                geojson_final = json.load(open(os.path.join(RESULTS_DIR, 'geojson_final.geojson')))
                g = [i for i in geojson_final['features'] if i['id'] == int(self.osm_id)]
                if g != []:
                    coord = g[0]['geometry']['coordinates']
                    wkt = 'SRID=4326;LINESTRING('
                    first = True

                    for c in coord:
                        if first:
                            first = False
                        else:
                            wkt = wkt + ','
                        wkt = wkt + str(c[0]) + ' ' + str(c[1])
                    wkt = wkt + ')'
                    self.geom = wkt
                else:
                    self.geom = 'SRID=4326;LINESTRING EMPTY'

        if self.previous_udate:
            geojson_ini = json.load(open(os.path.join(RESULTS_DIR, 'geojson_ini.geojson')))
            g = [i for i in geojson_ini['features'] if i['id'] == int(self.osm_id)]
            if g != []:
                coord = g[0]['geometry']['coordinates']
                wkt = 'SRID=4326;LINESTRING('
                first = True

                for c in coord:
                    if first:
                        first = False
                    else:
                        wkt = wkt + ','
                    wkt = wkt + str(c[0]) + ' ' + str(c[1])
                wkt = wkt + ')'
                self.previous_geom = wkt
            else:
                self.geom = 'SRID=4326;LINESTRING EMPTY'


Query.diff = relationship("Diff", order_by=Diff.id, back_populates="query")

if __name__ == '__main__':
    engine = get_engine()
    # create the db if not exists
    Base.metadata.create_all(engine)
