Change detection tree
=====================


The core of this tool is the change detection tree that analyses each augmented diff results.

The change detection tree of the *diffs* is schematized as follows:

*  create
    * geom
        * way_split
        * geom_creation
    * tag
        * tag_creation
* delete
    * geom
        * geom_deletion
    * tag
        * tag_deletion
* modify
    * geom
         * node_align
         * reverse_way
         * same_length_not_same_nodes
         * modif_geom
    * tag
         * watch_tag_modified
         * other_tag_modified

The first level of this tree is stored for each *diff* in the field `diff_type`, which takes `create`, `delete` and `modify` as values. The second level is stored in the field `diff_target`, which is `geom` or `tag`. The third level is stored in the field `diff_category`, which explains the type of the change that were detected.

Here's below are some real-OSM-world examples of changes that can to be detected using the tool. All this changes are related to the *watch_tag* `oneway=*`.

1) Creation of a new feature tagged with `oneway=*`:

* See the `changeset <http://www.openstreetmap.org/changeset/54205437>`_
* See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/tqX>`_
* See the change with `achavi <https://overpass-api.de/achavi/?changeset=54205437>`_

The geometry creation is shown by:

* the tag `<action type="create"></action>`
* the version of the object is always equal to 1: `<way version="1"></way>`
* there is a tag `oneway` inside the object `<way>`

1.1) way_split: A new OSM object is actually created whenever a contributor
split a way. See this `changeset <https://overpass-api.de/achavi/?changeset=56061465>`_ for instance, with this  `overpass query <http://overpass-turbo.eu/s/vRj>`_ where 2 streets were cut because the user wanted to create a bridge. In this case, the updated way segment and the new segment share a common node. This is by detecting this kind of "common nodes" that way-split are detected. NOTE that this test is not exclusive: if a way is split in more than two segments, it will not be detected by this test.


2) Creation of a new tag `oneway=*` on a existing object:

* See the `changeset <http://www.openstreetmap.org/changeset/54204968>`_
* See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/tqY>`_
* See the change with `achavi <https://overpass-api.de/achavi/?changeset=54204968>`_

The tag creation is shown by:

* the tag `<action type="create"></action>`
* the version of the object is always > 1: eg: `<way version="2"></way>`


3) Deletion of an object tagged with `oneway=*`:

* See the `changeset <http://www.openstreetmap.org/changeset/54205525>`_
* See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/tr5>`_
* See the change with `achavi <https://overpass-api.de/achavi/?changeset=54205525>`_

The geom deletion is shown by:

* the tag `<action type="delete"></action>`
* the object before the change, e.g.: `<old><way version="n"></way></old>`
* the object after the change, e.g.: `<new><way visible="false" version>"n+1"></way></new>` without the "oneway" tag


4) Deletion of a tag `oneway=*` on a existing object:

* See the `changeset <http://www.openstreetmap.org/changeset/54205105>`_
* See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/tr4>`_
* See the change with `achavi <https://overpass-api.de/achavi/?changeset=54205105>`_

The tag deletion is shown by:

* the tag `<action type="delete"></action>`
* the object before the change, e.g.: `<old><way version="n"></way></old>`
* the object after the change, e.g.: `<new><way visible="true" version>"n+1"></way></new>` without the "oneway" tag


5) Modification of the geometry of an object tagged with `oneway=*` :

* See the `changeset <http://www.openstreetmap.org/changeset/54205105>`_
* See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/tr6>`_
* See the change with `achavi <https://overpass-api.de/achavi/?changeset=54205105>`_

The geom modification is shown by:

* the tag `<action type="modify"></action>`
* the object before the change, e.g.: `<old><way version="n"></way></old>`
* the object after the change, e.g.: `<new><way version>"n+1"></way></new>`

5.1) If some nodes are added or deleted, then `<old><way></way></old>` and `<new><way></way></new>` are different, for instance this `changeset <http://www.openstreetmap.org/changeset/54205344), to see on (achavi)[https://overpass-api.de/achavi/?changeset=54205344].

5.2) node_align : If no nodes are added/deleted (but only moved), the nodes of the object `<way></way>` are the same between the old `<old><way></way></old>` and new `<new><way></way></new>` object. In the OSM community, this is often denominated as "alignment".

5.3) reverse_way: Sense of the way was reverted for an object tagged with `oneway=*`:

    * See the `changeset <https://www.openstreetmap.org/changeset/55649637>`_
    * See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/vb4>`_
    * See the change with `achavi <https://overpass-api.de/achavi/?changeset=55649637>`_

    The reverted way is shown by:

    * the tag `<action type="modify"></action>`
    * the object before the change, e.g.: `<old><way version="n"></way></old>`
    * the object before the change, e.g.: `<new><way version>"n+1"></way></new>` with the order of the nodes that is inverted.

6) Modification of a tag `oneway=*` on a existing object:

* See the `changeset <http://www.openstreetmap.org/changeset/54204793>`_
* See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/tr3>`_
* See the change with `achavi <https://overpass-api.de/achavi/?changeset=54204793>`_

The tag modification is shown by:

* the tag `<action type="modify"></action>`
* the object before the change, e.g.: `<old><way version="n"></way></old>`
* the object after the change, e.g.: `<new><way version>"n+1"></way></new>`



Combination of changes
------------------------

1) Create tag + modif geom

The following change is a tag addition (`oneway=no`) + a modification of geometry by adding one node.

* See the `changeset <https://www.openstreetmap.org/changeset/56094624>`_
* See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/vSt>`_
* See the change with `achavi <https://overpass-api.de/achavi/?changeset=56094624>`_

In this case, only the tag creation is reported by the query.


2) Modif tag + modif geom

The following change is a tag modification `oneway` + a modification of geometry by deleting one node.

* See the `changeset <https://www.openstreetmap.org/changeset/56095598>`_
* See the change using an `overpass-turbo query <http://overpass-turbo.eu/s/vSu>`_
* See the change with `achavi <https://overpass-api.de/achavi/?changeset=56095598>`_

In this case, the two modifications are simultaneously reported, with a version of the object before `<old></old>` and after `<new></new>` the change.
