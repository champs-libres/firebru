Run the tool
============



This tool analyses the `augmented diff <https://wiki.openstreetmap.org/wiki/Overpass_API/Augmented_Diffs>`_ results between two dates for a specific tag of OpenStreetMap. A *query* is defined as an augmented diff between two dates (*date_ini* & *date_final*) for a particular tag (namely, the *watch_tag*) over a certain area (namely, the *area*). Each *query* produces a number of *diffs* that are changes recorded for one particular OSM feature. Changes can be creation, deletion or modification of the geometry or tags of OSM features.

Interaction with the postgresql database is performed using `Geoachelmy <https://geoalchemy-2.readthedocs.io/>`_.


Run the analysis
----------------

Run `python adiff_change_detection.py` in command line. Do not forget to activate before the python virtual environment if you use this tool in a virtual environment. The tables must have been created before (run `python db.py`) running the tool.

or in a python shell:

.. code-block:: python

  from adiff_change_detection import change_detection
  change_detection(
      watch_tag = 'oneway',
      geom_type = 'way',
      area = '"name:fr" = "Bruxelles-Capitale"',
      date_ini = '2018-01-01T00:00:00Z',
      date_final = '2018-02-01T00:00:00Z',
      watch_tag_value_to_ignore = set(['no'])
  )



The script does the following:

1) This script makes some Overpass API requests using the `Overpass API python wrapper <https://github.com/mvexel/overpass-api-python-wrapper>`_ library. First, the augmented diff query is built and called using the `get_adiff_xml` function. Second, the geometries of the OSM features are retrieved at the *date_ini* & *date_final* dates making two overpass API calls using the `get_geojson` function.

2) The parameters of the query are stored in the table `query`.

3) The result of the augmented diff query are analysed using the `osmdiff <https://github.com/osmlab/osmdiff>`_ python library and our own tests. Basically, our tests extend the analysis made by osmdiff. The *diffs* are classified according the change detection tree (see below for a explanation of each tests on the *diff*).

4) Each *diff* is stored in the table `diff` with its initial and final geometries.

5) The script also writes a summary file `adiff_summary.txt` and to a logger `main.log`.


Parameters
----------

* watch_tag (required): the OSM tag that is analysed. `watch_tag = 'oneway'` is for `oneway=*`;
* watch_tag_value_to_ignore (optional): the OSM value that need to be ignored in the analysis. `watch_tag_value_to_ignore = set(['no']),` together with `watch_tag = 'oneway'` means that OSM objects tagged with `oneway=no` will not be analysed and classified by the tests.
* geom_type (required): the OSM type of object that is analysed. Must be be 'node', 'way' or 'relation';
* area (required): the search area of the analysis;
* date_ini: initial date of the analysis;
* date_final: final date of the analysis.

logging
-------
For logging the analysis, one may change the default logging level (CRITICAL) and set up, e.g., to DEBUG: `python adiff_change_detection.py -log=DEBUG`


About the overpass queries
--------------------------

This tool uses the `augmented diff <https://wiki.openstreetmap.org/wiki/Overpass_API/Augmented_Diffs>`_ functionnality of the overpass API. The augmented diff integrate the differences that may have occur between two dates for the OSM features that are queried. An augmented diff overpass query is as follows:

.. code-block:: sql

  [out:xml][timeout:250][adiff:"2017-09-01T00:00:00Z","2017-11-30T00:00:00Z"];
  {{geocodeArea:Bruxelles-Capitale}}->.searchArea;
  (
    way["oneway"](area.searchArea);
  );
  out meta;



Note that the `xml` output format is important here. One don't have the same information with other format of output. In overpass-turbo, this query does not load any geometries. You have to look at the data tab.

The overpass queries that are used for getting the geometries are similar, except that they target the OSM data at a specific date (using the `date` argument) and that the output format of the `Overpass API python wrapper <https://github.com/mvexel/overpass-api-python-wrapper>`_ is 'geojson'.
