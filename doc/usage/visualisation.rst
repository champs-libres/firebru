Visualisation
=============

With QGIS
-------

Some `qml` files were set up for visualising the two layers (actual and previous geometries) that result from the adiff_change_detection run. These style files are useful for filtering results. There are in map/qgis.

With MapServer
---------------

Basic map-files for setting up WMS and WFS for these two layers were written in map/mapserver.

With OpenLayers
---------------

A basic OpenLayers application was set up to visualize the WFS set up with MapServer. It can be the starting point for building a web application. It is in map/ol.
