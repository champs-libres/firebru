Usage
=====

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   usage/run
   usage/change_detection_tree
   usage/visualisation
