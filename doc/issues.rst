Known issues
==============

overpass.errors.MultipleRequestsError
--------------------------------------

Sometimes, the Overpass server is busy wit too many / heavy requests and sends back this error: overpass.errors.MultipleRequestsError. The current solution is to manually relaunch the script.
A better solution would be to automatically relaunch the script after a few second. 

See this related issue on the overpass API python wrapper: https://github.com/mvexel/overpass-api-python-wrapper/issues/88
