Installation
==============

Create a virtual environment, using python 3
----------------------------------------------
```
virtualenv -p python3 venv
```

Activate the virtual environment
-----------------------------------
```
source venv/bin/activate
```

Install requirements
-----------------------
```
pip install -r requirements.txt
```

Create a postgis db
-------------------

* via docker, for instance
```
docker run --name firebru-diff-db -p 5432 -d mdillon/postgis
```

Parameter the db
-----------------
Copy the `parameters.py.dist` into `parameters.py` and edit the file `parameters.py` with local credentials. If the db was created via docker, type `docker ps` to know the port in use.

Create the tables in the db
---------------------------
Run `python db.py`
