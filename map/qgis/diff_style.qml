<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="1" readOnly="0" simplifyDrawingHints="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" maxScale="0" simplifyLocal="1" version="3.1.0-Master" minScale="1e+08" simplifyAlgorithm="0" simplifyMaxScale="1">
  <renderer-v2 forceraster="0" type="RuleRenderer" enableorderby="0" symbollevels="0">
    <rules key="{0b8563e3-d28c-4c2e-9d1e-5a511ddb730a}">
      <rule filter="&quot;diff_type&quot; = 'create'" key="{fb3b0279-0f4f-4da8-acac-d0481ca12b8b}" label="create">
        <rule filter="&quot;diff_target&quot; = 'geom'" key="{cfb7ff94-f0e0-440f-8fee-6ce7ce19b342}" label="geom">
          <rule filter=" ( &quot;diff_category&quot; = 'oneway=no' ) AND ( &quot;diff_target&quot; = 'geom' ) AND ( &quot;diff_type&quot; = 'create' ) " key="{76b6daf1-86b7-4a83-afa6-dbc0db341430}" symbol="0" label="oneway=no"/>
          <rule filter="&quot;diff_category&quot; = 'geom_creation'" key="{7c0eda15-9c1c-4869-ac9d-183b740340f5}" symbol="1" label="&quot;diff_category&quot; = 'geom_creation'"/>
          <rule filter="&quot;diff_category&quot; = 'way_split'" key="{81b048b0-d90a-4775-99eb-f5dca296345d}" symbol="2" label="&quot;diff_category&quot; = 'way_split'"/>
          <rule filter="&quot;diff_category&quot; = 'oneway=no'" key="{3d80d479-6eee-448e-8ad7-4a801d702c17}" symbol="3" label="&quot;diff_category&quot; = 'oneway=no'"/>
        </rule>
        <rule filter="&quot;diff_target&quot; = 'tag'" key="{573163d6-9266-4ea3-be6e-eab8717c1ea2}" label="tag">
          <rule filter="&quot;diff_category&quot; = 'tag_creation'" key="{0b3ec480-7704-4b15-a261-b1ccf80da388}" symbol="4" label="&quot;diff_category&quot; = 'tag_creation'"/>
          <rule filter=" ( &quot;diff_category&quot; = 'oneway=no' ) AND ( &quot;diff_target&quot; = 'tag' ) AND ( &quot;diff_type&quot; = 'create' ) " key="{f5afba4e-8fcd-4bd9-af30-abe1ac97b652}" symbol="5" label="oneway=no"/>
        </rule>
      </rule>
      <rule filter="&quot;diff_type&quot; = 'modify'" key="{79d904da-5344-4337-9f2a-aa8287e2b01c}" label="modify">
        <rule filter="&quot;diff_target&quot; = 'geom'" key="{566835ff-9985-4d60-b1b6-e134b4ba2e2f}" label="geom">
          <rule filter="&quot;diff_category&quot; = 'modif_geom'" key="{885d02dd-4680-4d6f-954d-c3c375b58ec2}" symbol="6" label="&quot;diff_category&quot; = 'modif_geom'"/>
          <rule filter="&quot;diff_category&quot; = 'node_align'" key="{49ee3d36-ee13-4f7d-b704-7c3d5b53d625}" symbol="7" label="&quot;diff_category&quot; = 'node_align'"/>
          <rule filter="&quot;diff_category&quot; = 'reverse_way'" key="{22523a09-8049-4990-bab9-f44774cdb6c2}" symbol="8" label="&quot;diff_category&quot; = 'reverse_way'"/>
          <rule filter="&quot;diff_category&quot; = 'same_length_not_same_nodes'" key="{d497040a-48e4-4dbc-a64f-c698d0d3a743}" symbol="9" label="&quot;diff_category&quot; = 'same_length_not_same_nodes'"/>
        </rule>
        <rule filter="&quot;diff_target&quot; = 'tag'" key="{6fdbf600-c835-4f47-8f28-59f8c4b1ec9e}" label="tag">
          <rule filter="&quot;diff_category&quot; = 'other_tag_modified'" key="{438bfc1f-ae17-41f8-a0af-8ac8f326c248}" symbol="10" label="&quot;diff_category&quot; = 'other_tag_modified'"/>
          <rule filter="&quot;diff_category&quot; = 'watch_tag_modified'" key="{7b378b4e-21b1-45ca-99a8-00be7b7ed91e}" symbol="11" label="&quot;diff_category&quot; = 'watch_tag_modified'"/>
        </rule>
      </rule>
    </rules>
    <symbols>
      <symbol alpha="1" name="0" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,255,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="1" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,255,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="10" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="11" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="2" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,255,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="3" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,255,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="4" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,255,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="5" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,255,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="6" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="7" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="8" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="9" type="line" clip_to_extent="1">
        <layer class="SimpleLine" enabled="1" locked="0" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{3daf384a-2110-4814-912a-d5d971cab39c}">
      <rule key="{9de479c8-e117-4fb1-a7f7-d700b01ddf67}" active="0">
        <settings>
          <text-style textOpacity="1" fontItalic="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" textColor="0,0,0,255" fontLetterSpacing="0" fontFamily="Ubuntu" fontWeight="50" isExpression="0" multilineHeight="1" fontCapitals="0" previewBkgrdColor="#ffffff" blendMode="0" fontSize="10" fontStrikeout="0" useSubstitutions="0" fieldName="diff_category" fontSizeUnit="Point" namedStyle="Regular" fontWordSpacing="0" fontUnderline="0">
            <text-buffer bufferSize="1" bufferJoinStyle="128" bufferBlendMode="0" bufferOpacity="1" bufferDraw="0" bufferNoFill="1" bufferSizeUnits="MM" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferColor="255,255,255,255"/>
            <background shapeSizeX="0" shapeRadiiX="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeRotation="0" shapeSVGFile="" shapeOffsetX="0" shapeRotationType="0" shapeBorderWidth="0" shapeBorderWidthUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeBlendMode="0" shapeSizeType="0" shapeOffsetY="0" shapeDraw="0" shapeSizeY="0" shapeBorderColor="128,128,128,255" shapeType="0" shapeOpacity="1" shapeFillColor="255,255,255,255" shapeRadiiUnit="MM" shapeSizeUnit="MM" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeJoinStyle="64" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeOffsetUnit="MM"/>
            <shadow shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0" shadowOffsetDist="1" shadowOffsetUnit="MM" shadowOpacity="0.7" shadowBlendMode="6" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowScale="100" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowUnder="0" shadowColor="0,0,0,255" shadowRadius="1.5" shadowRadiusUnit="MM" shadowOffsetAngle="135"/>
            <substitutions/>
          </text-style>
          <text-format leftDirectionSymbol="&lt;" plussign="0" addDirectionSymbol="0" rightDirectionSymbol=">" reverseDirectionSymbol="0" decimals="3" placeDirectionSymbol="0" formatNumbers="0" multilineAlign="4294967295" wrapChar=""/>
          <placement rotationAngle="0" quadOffset="4" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" distUnits="MM" maxCurvedCharAngleIn="25" placementFlags="10" xOffset="0" fitInPolygonOnly="0" priority="5" dist="0" placement="2" centroidWhole="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceUnits="MM" yOffset="0" maxCurvedCharAngleOut="-25" offsetUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" repeatDistance="0" offsetType="0" distMapUnitScale="3x:0,0,0,0,0,0" centroidInside="0" preserveRotation="1"/>
          <rendering fontLimitPixelSize="0" limitNumLabels="0" scaleMin="0" labelPerPart="0" obstacleType="0" maxNumLabels="2000" obstacle="1" mergeLines="0" drawLabels="1" upsidedownLabels="0" scaleVisibility="0" scaleMax="0" minFeatureSize="0" obstacleFactor="1" fontMinPixelSize="3" displayAll="0" zIndex="0" fontMaxPixelSize="10000"/>
          <dd_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </dd_properties>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE( "id", '&lt;NULL>' )</value>
      <value>COALESCE( "id", '&lt;NULL>' )</value>
      <value>"id"</value>
      <value>COALESCE( "id", '&lt;NULL>' )</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory sizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" minimumSize="0" height="15" maxScaleDenominator="1e+08" width="15" labelPlacementMethod="XHeight" opacity="1" scaleDependency="Area" minScaleDenominator="0" enabled="0" penWidth="0" barWidth="5" penAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" backgroundAlpha="255" penColor="#000000" diagramOrientation="Up" rotationOffset="270" scaleBasedVisibility="0" sizeType="MM">
      <fontProperties style="" description="Ubuntu,11,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" dist="0" linePlacementFlags="18" placement="2" zIndex="0" obstacle="0" priority="0">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="changeset_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="udate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_udate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_geom">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tag_value">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_tag_value">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diff_type">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diff_target">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diff_category">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="query_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id"/>
    <alias name="" index="1" field="osm_id"/>
    <alias name="" index="2" field="changeset_id"/>
    <alias name="" index="3" field="udate"/>
    <alias name="" index="4" field="previous_udate"/>
    <alias name="" index="5" field="previous_geom"/>
    <alias name="" index="6" field="tag_value"/>
    <alias name="" index="7" field="previous_tag_value"/>
    <alias name="" index="8" field="diff_type"/>
    <alias name="" index="9" field="diff_target"/>
    <alias name="" index="10" field="diff_category"/>
    <alias name="" index="11" field="query_id"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="osm_id" applyOnUpdate="0"/>
    <default expression="" field="changeset_id" applyOnUpdate="0"/>
    <default expression="" field="udate" applyOnUpdate="0"/>
    <default expression="" field="previous_udate" applyOnUpdate="0"/>
    <default expression="" field="previous_geom" applyOnUpdate="0"/>
    <default expression="" field="tag_value" applyOnUpdate="0"/>
    <default expression="" field="previous_tag_value" applyOnUpdate="0"/>
    <default expression="" field="diff_type" applyOnUpdate="0"/>
    <default expression="" field="diff_target" applyOnUpdate="0"/>
    <default expression="" field="diff_category" applyOnUpdate="0"/>
    <default expression="" field="query_id" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" exp_strength="0" unique_strength="1" field="id" notnull_strength="1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="osm_id" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="changeset_id" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="udate" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="previous_udate" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="previous_geom" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="tag_value" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="previous_tag_value" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="diff_type" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="diff_target" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="diff_category" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="query_id" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id"/>
    <constraint desc="" exp="" field="osm_id"/>
    <constraint desc="" exp="" field="changeset_id"/>
    <constraint desc="" exp="" field="udate"/>
    <constraint desc="" exp="" field="previous_udate"/>
    <constraint desc="" exp="" field="previous_geom"/>
    <constraint desc="" exp="" field="tag_value"/>
    <constraint desc="" exp="" field="previous_tag_value"/>
    <constraint desc="" exp="" field="diff_type"/>
    <constraint desc="" exp="" field="diff_target"/>
    <constraint desc="" exp="" field="diff_category"/>
    <constraint desc="" exp="" field="query_id"/>
  </constraintExpressions>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;diff_category&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" name="id" width="-1" type="field"/>
      <column hidden="0" name="osm_id" width="-1" type="field"/>
      <column hidden="0" name="udate" width="-1" type="field"/>
      <column hidden="0" name="previous_udate" width="-1" type="field"/>
      <column hidden="0" name="diff_type" width="-1" type="field"/>
      <column hidden="0" name="diff_target" width="-1" type="field"/>
      <column hidden="0" name="diff_category" width="169" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
      <column hidden="0" name="changeset_id" width="162" type="field"/>
      <column hidden="0" name="previous_geom" width="126" type="field"/>
      <column hidden="0" name="tag_value" width="-1" type="field"/>
      <column hidden="0" name="previous_tag_value" width="-1" type="field"/>
      <column hidden="0" name="query_id" width="-1" type="field"/>
    </columns>
  </attributetableconfig>
  <editform>/media/julien/data/ChampsLibres/Projets/FireBru/change_detection/gis</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>/media/julien/data/ChampsLibres/Projets/FireBru/change_detection/gis</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="changeset_id" editable="1"/>
    <field name="diff_category" editable="1"/>
    <field name="diff_target" editable="1"/>
    <field name="diff_type" editable="1"/>
    <field name="id" editable="1"/>
    <field name="osm_id" editable="1"/>
    <field name="osm_tag" editable="1"/>
    <field name="osm_type" editable="1"/>
    <field name="previous_geom" editable="1"/>
    <field name="previous_tag_value" editable="1"/>
    <field name="previous_udate" editable="1"/>
    <field name="query_id" editable="1"/>
    <field name="tag_value" editable="1"/>
    <field name="udate" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="changeset_id" labelOnTop="0"/>
    <field name="diff_category" labelOnTop="0"/>
    <field name="diff_target" labelOnTop="0"/>
    <field name="diff_type" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="osm_id" labelOnTop="0"/>
    <field name="osm_tag" labelOnTop="0"/>
    <field name="osm_type" labelOnTop="0"/>
    <field name="previous_geom" labelOnTop="0"/>
    <field name="previous_tag_value" labelOnTop="0"/>
    <field name="previous_udate" labelOnTop="0"/>
    <field name="query_id" labelOnTop="0"/>
    <field name="tag_value" labelOnTop="0"/>
    <field name="udate" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <expressionfields/>
  <previewExpression>COALESCE( "id", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
