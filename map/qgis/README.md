OpenStreetMap change detection tool - qgis
==========================================

This folder contains some qgis layer style files (`.qml`) for visualizing the diff layers. The `diff_style.qml` is intended for styling the last geometries (column `geom` in table `diff`). The `diff_previous_style.qml` is intended for the previous geometries  (column `previous_geom` in table `diff`). 
