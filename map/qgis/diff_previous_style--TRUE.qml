<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="1" simplifyDrawingTol="1" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="1" simplifyMaxScale="1" minScale="1e+08" version="3.1.0-Master" maxScale="0" simplifyLocal="1" readOnly="0">
  <renderer-v2 forceraster="0" enableorderby="0" type="RuleRenderer" symbollevels="0">
    <rules key="{0b8563e3-d28c-4c2e-9d1e-5a511ddb730a}">
      <rule label="delete" key="{608de187-991b-42d4-b5bd-42e978604238}" filter="&quot;diff_type&quot; = 'delete'">
        <rule label="tag" key="{cfa6e3ed-21b8-4d02-a847-2e9813c9d9b1}" filter="&quot;diff_target&quot; = 'tag'">
          <rule label="oneway=no" key="{92df2bdf-c0ad-4b13-a49b-5438a8734daf}" checkstate="0" filter="&quot;diff_category&quot; = 'oneway=no' AND &quot;diff_target&quot; = 'tag' AND &quot;diff_type&quot; = 'delete' " symbol="0"/>
          <rule label="&quot;diff_category&quot; = 'tag_deletion'" key="{c33e4251-5f44-462a-83ed-f8b62f837b13}" filter="&quot;diff_category&quot; = 'tag_deletion'" symbol="1"/>
        </rule>
        <rule label="geom" key="{b4e448a8-843c-4c62-b552-01f9bd68f8b6}" filter="&quot;diff_target&quot; = 'geom'">
          <rule label="&quot;diff_category&quot; = 'geom_deletion'" key="{c95d98c7-b336-48c7-9957-85c533ff3263}" filter="&quot;diff_category&quot; = 'geom_deletion'" symbol="2"/>
          <rule label="oneway=no" key="{b27f4347-85a8-49d9-8acc-36faf158aeaf}" checkstate="0" filter="&quot;diff_category&quot; = 'oneway=no' AND &quot;diff_target&quot; = 'geom' AND &quot;diff_type&quot; = 'delete' " symbol="3"/>
        </rule>
      </rule>
      <rule label="modify" key="{79d904da-5344-4337-9f2a-aa8287e2b01c}" filter="&quot;diff_type&quot; = 'modify'">
        <rule label="geom" key="{566835ff-9985-4d60-b1b6-e134b4ba2e2f}" filter="&quot;diff_target&quot; = 'geom'">
          <rule label="&quot;diff_category&quot; = 'modif_geom'" key="{885d02dd-4680-4d6f-954d-c3c375b58ec2}" checkstate="0" filter="&quot;diff_category&quot; = 'modif_geom'" symbol="4"/>
          <rule label="&quot;diff_category&quot; = 'node_align'" key="{49ee3d36-ee13-4f7d-b704-7c3d5b53d625}" checkstate="0" filter="&quot;diff_category&quot; = 'node_align'" symbol="5"/>
          <rule label="&quot;diff_category&quot; = 'reverse_way'" key="{22523a09-8049-4990-bab9-f44774cdb6c2}" filter="&quot;diff_category&quot; = 'reverse_way'" symbol="6"/>
          <rule label="&quot;diff_category&quot; = 'same_length_not_same_nodes'" key="{d497040a-48e4-4dbc-a64f-c698d0d3a743}" checkstate="0" filter="&quot;diff_category&quot; = 'same_length_not_same_nodes'" symbol="7"/>
        </rule>
        <rule label="tag" key="{6fdbf600-c835-4f47-8f28-59f8c4b1ec9e}" filter="&quot;diff_target&quot; = 'tag'">
          <rule label="&quot;diff_category&quot; = 'other_tag_modified'" key="{438bfc1f-ae17-41f8-a0af-8ac8f326c248}" checkstate="0" filter="&quot;diff_category&quot; = 'other_tag_modified'" symbol="8"/>
          <rule label="&quot;diff_category&quot; = 'watch_tag_modified'" key="{7b378b4e-21b1-45ca-99a8-00be7b7ed91e}" filter="&quot;diff_category&quot; = 'watch_tag_modified'" symbol="9"/>
        </rule>
      </rule>
    </rules>
    <symbols>
      <symbol type="line" name="0" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="231,7,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="1" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="231,7,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="2" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="231,7,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="3" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="231,7,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="4" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="5" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="6" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="7" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="8" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" name="9" alpha="1" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,196,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{d8bb3428-1d6d-42a4-b88d-e25a6bac90b9}">
      <rule active="0" key="{1ec607bd-bb49-4dd5-a5fa-d0eb8c68e9d1}">
        <settings>
          <text-style blendMode="0" fontItalic="0" fieldName="diff_category" fontWeight="50" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontLetterSpacing="0" fontWordSpacing="0" fontCapitals="0" textColor="0,0,0,255" fontFamily="Ubuntu" namedStyle="Regular" fontSize="10" previewBkgrdColor="#ffffff" isExpression="0" fontUnderline="0" useSubstitutions="0" textOpacity="1" fontStrikeout="0" fontSizeUnit="Point" multilineHeight="1">
            <text-buffer bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferJoinStyle="128" bufferNoFill="1" bufferSize="1" bufferSizeUnits="MM" bufferColor="255,255,255,255" bufferOpacity="1" bufferBlendMode="0" bufferDraw="0"/>
            <background shapeBorderWidthUnit="MM" shapeRotationType="0" shapeBorderColor="128,128,128,255" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="MM" shapeSizeType="0" shapeType="0" shapeJoinStyle="64" shapeRadiiUnit="MM" shapeSVGFile="" shapeRotation="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeRadiiY="0" shapeOpacity="1" shapeFillColor="255,255,255,255" shapeBlendMode="0" shapeRadiiX="0" shapeBorderWidth="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetY="0" shapeDraw="0" shapeOffsetX="0" shapeSizeY="0"/>
            <shadow shadowOffsetGlobal="1" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusAlphaOnly="0" shadowBlendMode="6" shadowDraw="0" shadowRadius="1.5" shadowRadiusUnit="MM" shadowColor="0,0,0,255" shadowOffsetDist="1" shadowScale="100" shadowOffsetAngle="135" shadowOffsetUnit="MM" shadowOpacity="0.7" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowUnder="0"/>
            <substitutions/>
          </text-style>
          <text-format multilineAlign="4294967295" leftDirectionSymbol="&lt;" placeDirectionSymbol="0" plussign="0" wrapChar="" addDirectionSymbol="0" reverseDirectionSymbol="0" rightDirectionSymbol=">" formatNumbers="0" decimals="3"/>
          <placement distUnits="MM" centroidInside="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" dist="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" priority="5" placementFlags="10" centroidWhole="0" maxCurvedCharAngleOut="-25" placement="2" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" repeatDistanceUnits="MM" quadOffset="4" rotationAngle="0" offsetUnits="MM" fitInPolygonOnly="0" offsetType="0" xOffset="0" distMapUnitScale="3x:0,0,0,0,0,0" preserveRotation="1" maxCurvedCharAngleIn="25" repeatDistance="0" yOffset="0"/>
          <rendering fontMaxPixelSize="10000" upsidedownLabels="0" minFeatureSize="0" maxNumLabels="2000" fontMinPixelSize="3" fontLimitPixelSize="0" drawLabels="1" labelPerPart="0" limitNumLabels="0" obstacleFactor="1" zIndex="0" obstacleType="0" scaleMin="0" displayAll="0" scaleMax="0" obstacle="1" mergeLines="0" scaleVisibility="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </dd_properties>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penAlpha="255" backgroundColor="#ffffff" sizeType="MM" rotationOffset="270" scaleBasedVisibility="0" barWidth="5" lineSizeType="MM" maxScaleDenominator="1e+08" height="15" diagramOrientation="Up" backgroundAlpha="255" width="15" minimumSize="0" minScaleDenominator="0" lineSizeScale="3x:0,0,0,0,0,0" sizeScale="3x:0,0,0,0,0,0" penWidth="0" scaleDependency="Area" labelPlacementMethod="XHeight" opacity="1" penColor="#000000" enabled="0">
      <fontProperties style="" description="Ubuntu,11,-1,5,50,0,0,0,0,0"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" priority="0" placement="2" linePlacementFlags="18" dist="0" obstacle="0" zIndex="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="changeset_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="udate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_udate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geom">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tag_value">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_tag_value">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diff_type">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diff_target">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diff_category">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="query_id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id"/>
    <alias name="" index="1" field="osm_id"/>
    <alias name="" index="2" field="changeset_id"/>
    <alias name="" index="3" field="udate"/>
    <alias name="" index="4" field="previous_udate"/>
    <alias name="" index="5" field="geom"/>
    <alias name="" index="6" field="tag_value"/>
    <alias name="" index="7" field="previous_tag_value"/>
    <alias name="" index="8" field="diff_type"/>
    <alias name="" index="9" field="diff_target"/>
    <alias name="" index="10" field="diff_category"/>
    <alias name="" index="11" field="query_id"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="osm_id" applyOnUpdate="0"/>
    <default expression="" field="changeset_id" applyOnUpdate="0"/>
    <default expression="" field="udate" applyOnUpdate="0"/>
    <default expression="" field="previous_udate" applyOnUpdate="0"/>
    <default expression="" field="geom" applyOnUpdate="0"/>
    <default expression="" field="tag_value" applyOnUpdate="0"/>
    <default expression="" field="previous_tag_value" applyOnUpdate="0"/>
    <default expression="" field="diff_type" applyOnUpdate="0"/>
    <default expression="" field="diff_target" applyOnUpdate="0"/>
    <default expression="" field="diff_category" applyOnUpdate="0"/>
    <default expression="" field="query_id" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="3" unique_strength="1" notnull_strength="1" field="id" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="osm_id" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="changeset_id" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="udate" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="previous_udate" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="geom" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="tag_value" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="previous_tag_value" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="diff_type" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="diff_target" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="diff_category" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" field="query_id" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="osm_id"/>
    <constraint exp="" desc="" field="changeset_id"/>
    <constraint exp="" desc="" field="udate"/>
    <constraint exp="" desc="" field="previous_udate"/>
    <constraint exp="" desc="" field="geom"/>
    <constraint exp="" desc="" field="tag_value"/>
    <constraint exp="" desc="" field="previous_tag_value"/>
    <constraint exp="" desc="" field="diff_type"/>
    <constraint exp="" desc="" field="diff_target"/>
    <constraint exp="" desc="" field="diff_category"/>
    <constraint exp="" desc="" field="query_id"/>
  </constraintExpressions>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="1" sortExpression="&quot;id&quot;" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" name="id" hidden="0" width="-1"/>
      <column type="field" name="osm_id" hidden="0" width="-1"/>
      <column type="field" name="udate" hidden="0" width="-1"/>
      <column type="field" name="previous_udate" hidden="0" width="-1"/>
      <column type="field" name="diff_type" hidden="0" width="-1"/>
      <column type="field" name="diff_target" hidden="0" width="-1"/>
      <column type="field" name="diff_category" hidden="0" width="169"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" name="changeset_id" hidden="0" width="61"/>
      <column type="field" name="tag_value" hidden="0" width="-1"/>
      <column type="field" name="previous_tag_value" hidden="0" width="-1"/>
      <column type="field" name="geom" hidden="0" width="-1"/>
      <column type="field" name="query_id" hidden="0" width="-1"/>
    </columns>
  </attributetableconfig>
  <editform>/media/julien/data/ChampsLibres/Projets/FireBru/change_detection/gis</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>/media/julien/data/ChampsLibres/Projets/FireBru/change_detection/gis</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="changeset_id"/>
    <field editable="1" name="diff_category"/>
    <field editable="1" name="diff_target"/>
    <field editable="1" name="diff_type"/>
    <field editable="1" name="geom"/>
    <field editable="1" name="id"/>
    <field editable="1" name="osm_id"/>
    <field editable="1" name="osm_tag"/>
    <field editable="1" name="osm_type"/>
    <field editable="1" name="previous_geom"/>
    <field editable="1" name="previous_tag_value"/>
    <field editable="1" name="previous_udate"/>
    <field editable="1" name="query_id"/>
    <field editable="1" name="tag_value"/>
    <field editable="1" name="udate"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="changeset_id"/>
    <field labelOnTop="0" name="diff_category"/>
    <field labelOnTop="0" name="diff_target"/>
    <field labelOnTop="0" name="diff_type"/>
    <field labelOnTop="0" name="geom"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="osm_id"/>
    <field labelOnTop="0" name="osm_tag"/>
    <field labelOnTop="0" name="osm_type"/>
    <field labelOnTop="0" name="previous_geom"/>
    <field labelOnTop="0" name="previous_tag_value"/>
    <field labelOnTop="0" name="previous_udate"/>
    <field labelOnTop="0" name="query_id"/>
    <field labelOnTop="0" name="tag_value"/>
    <field labelOnTop="0" name="udate"/>
  </labelOnTop>
  <widgets/>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <expressionfields/>
  <previewExpression>COALESCE( "id", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
