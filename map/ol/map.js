var mapserver = 'http://localhost/cgi-bin/mapserv?map=/var/www/html/FireBru/mapserver/diff_wfs.map&';

var wfsSourceGeom = new ol.source.Vector({
  format: new ol.format.WFS(),
  url: function(extent) {
    return mapserver +
        'service=WFS&version=1.1.0&request=GetFeature&' +
        'typename=diff_geom&' +
        'srsname=EPSG:3857&' +
        'bbox=' + extent.join(',') + ',EPSG:3857';
  },
  strategy: ol.loadingstrategy.bbox
});

var wfsSourcePreviousGeom = new ol.source.Vector({
  format: new ol.format.WFS(),
  url: function(extent) {
    return mapserver +
        'service=WFS&version=1.1.0&request=GetFeature&' +
        'typename=diff_previous_geom&' +
        'srsname=EPSG:3857&' +
        'bbox=' + extent.join(',') + ',EPSG:3857';
  },
  strategy: ol.loadingstrategy.bbox
});

var getColor = function(feature){
    var color;
    if (feature.values_.diff_type === 'create'){
        color = 'rgba(0, 255, 0, 1.0)';
    }
    if (feature.values_.diff_type === 'delete'){
        color = 'rgba(255, 25, 0, 1.0)';
    }
    if (feature.values_.diff_type === 'modify'){
        color = 'rgba(255, 180, 0, 1.0)';
    }
    return color;
};

var getLineDash = function(feature){
    var lineDash;
    if (feature.values_.diff_target === 'geom'){
        lineDash = [4];
    }
    if (feature.values_.diff_target === 'tag'){
        lineDash = [0];
    }
    return lineDash;
};

var getStyleWFS = function(feature, resolution){
    var color, lineDash, style;

    color = getColor(feature);
    lineDash = getLineDash(feature);
    style = new ol.style.Style({
        stroke: new ol.style.Stroke({
        color: color,
        lineDash: lineDash,
        width: 2
        })
    });
    return style;
};

var geom = new ol.layer.Vector({
  source: wfsSourceGeom,
  style: getStyleWFS
});

var previousGeom = new ol.layer.Vector({
  source: wfsSourcePreviousGeom,
  style: getStyleWFS
});

var osm = new ol.layer.Tile({
    //source: new ol.source.OSM()
    source: new ol.source.Stamen({
        layer: 'toner'
    })
});

var map = new ol.Map({
  layers: [osm, geom, previousGeom],
  target: document.getElementById('map'),
  view: new ol.View({
    center: [490000, 6590000],
    maxZoom: 19,
    zoom: 11
  })
});
